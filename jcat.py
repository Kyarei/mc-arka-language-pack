#!/usr/bin/env python3

import json
import os
import sys

fh = open(sys.argv[1])

my_json = json.load(fh)

json.dump(my_json, sys.stdout, indent=2, ensure_ascii=False)
